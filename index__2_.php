<?php include 'header.php'; ?>

    <h1>Mahasiswa</h1>

    <?php if (!empty($_SESSION['username'])) { ?>

    <a href="formMahasiswa.php" class="btn btn-info btn-sm mt-3 mb-0">Tambah</a>

    <?php } ?>

    <table class="table container mt-0">
        <thead class="table-primary">
        <tr>
            <th>No.</th>
            <th>NIM</th>
            <th>Nama</th>
            <th>Jenis Kelamin</th>
            <th>Tanggal Lahir</th>
            <th>Alamat</th>
            <th>Program Studi</th>

            <?php if (!empty($_SESSION['username'])) { ?>

            <th>Aksi</th>

            <?php } ?>

        </tr>
    </thead>
    <tbody>

        <?php 
            $sql = 'SELECT * FROM mahasiswa JOIN prodi ON prodi.id_prodi = mahasiswa.id_prodi';

            $query = mysqli_query($conn, $sql);

            $i = 1;

            while ($row = mysqli_fetch_object($query)) {
        ?>
        
        <tr>
            <td><?php echo $i++; ?></td>
            <td><?php echo $row->nim; ?></td>
            <td><?php echo $row->nama; ?></td>
            <td><?php echo $row->jenis_kelamin; ?></td>
            <td><?php echo $row->tanggal_lahir; ?></td>
            <td><?php echo $row->alamat; ?></td>
            <td><?php echo $row->nama_prodi; ?></td>

            <?php if (!empty($_SESSION['username'])) { ?>

            <td>
                <a href="formMahasiswa.php?nim=<?php echo $row->nim; ?>" class="btn btn-warning btn-sm">Ubah</a>
                <a href="deleteMahasiswa.php?nim=<?php echo $row->nim; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin akan menghapus data?');">Hapus</a>
            </td>

            <?php } ?>

        </tr>

        <?php
            }

            if (! mysqli_num_rows($query)) {
                echo '<tr><td colspan="8" class="text-center">Tidak ada data.</td></tr>';
            }
        ?>
    </tbody>
    </table>

<?php include 'footer.php'; ?>