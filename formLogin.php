<?php include 'header.php'; ?>

<h1>Form Login</h1>
<form method="post" action="login.php">
    <div class="mb-3">
        <label class="form-label">Username</label>
        <input type="text" class="form-control" placeholder="Username" name="username" required>
    </div>
    <div class="mb-3">
        <label class="form-label">Password</label>
        <input type="password" class="form-control" placeholder="Password" name="password" required>
    </div>
    <div class="mb-3">
        <input type="submit" class="btn btn-success" value="Masuk">
    </div>
</form>

<?php include 'footer.php'; ?>