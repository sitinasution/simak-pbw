<?php include 'header.php'?>
<h1>Prodi Mahasiswa</h1>
        <a href="formProdi.php" class="btn btn-info btn-sm mt-3 mb-0">Tambah</a>
    </div>
    <table class="table container mt-0">
        <thead class="table-primary">
        <tr>
            <th>No.</th>
            <th>Id Prodi</th>
            <th>Nama Prodi</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>

        <?php 
            $sql = 'SELECT * FROM prodi';

            $query = mysqli_query($conn, $sql);

            $i = 1;

            while ($row = mysqli_fetch_object($query)) {
        ?>
        
        <tr>
            <td><?php echo $i++; ?></td>
            <td><?php echo $row->id_prodi; ?></td>
            <td><?php echo $row->nama_prodi; ?></td>
            <td>
                <a href="deleteProdi.php?id_prodi=<?php echo $row->id_prodi; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin akan menghapus data?');">Hapus</a>
            </td>
        </tr>

        <?php
            }

            if (! mysqli_num_rows($query)) {
                echo '<tr><td colspan="8" class="text-center">Tidak ada data.</td></tr>';
            }
        ?>
    </tbody>
    </table>




<?php include 'footer.php'?>