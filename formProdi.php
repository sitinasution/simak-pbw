<?php include 'header.php'; ?>

<h1>Form Prodi</h1>
<form method="post" action="saveProdi.php">
    <div class="mb-3">
        <label class="form-label">Id Prodi</label>
        <input type="text" class="form-control" placeholder="Id Prodi" name="id_prodi" required>
    </div>
    <div class="mb-3">
        <label class="form-label">Nama Prodi</label>
        <input type="text" class="form-control" placeholder="Nama Prodi" name="nama_prodi" required>
    </div>    
    <div class="mb-3">
        <input type="submit" class="btn btn-success" value="Simpan">
    </div>
</form>

<?php include 'footer.php'; ?>